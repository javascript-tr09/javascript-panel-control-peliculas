//VARIABLE DE ESTADO
const LIST = 'list';
const EDIT = 'edit';

var estado = LIST;


//BOTON GUARDAR / ACTUALIZAR
btnGuardar = document.querySelector('#guardar');
btnGuardar.onclick = function() {
    switch (estado) {
        case LIST:
            crearPelicula();
            break;

        case EDIT:
            actualizarPelicula();
            break;

        default:
            LIST;
    };
};

//LISTADO DE PELÍCULAS
var peliculas = [
    { 
        "id" : 1,
        "nombre"  :  "El señor de los anillos", 
        "genero"   :  "Accion",
        "anio" : "2010"
    },
    { 
        "id" : 2,
        "nombre"  :  "Regreso al Futuro", 
        "genero"   :  "Accion",
        "anio" : "1982"
    },
    {
        "id" : 3,
        "nombre"  :  "Casablanca", 
        "genero"   :  "Suspense",
        "anio" : "1949"
    }
];

//CREAR PELÍCULA
crearPelicula = function() {

    //SACO EL ULTIMO ID Y LO PARSEO PARA TRAERME UN NUMERO
    var saberId = document.querySelector('tbody').lastChild.firstChild.textContent;
    //console.log(typeof(saberId));  SACO EL TIPO DE EL ID
    var idnum = parseInt(saberId);
    //console.log(typeof(idnum));  LO CAMBIO DE STRING A NUMBER

    //VARIABLES PARA PODER INSERTAR EN LOS CAMPOS
    var insertTitulo = document.querySelector('#tituloForm');
    var insertGenero = document.querySelector('#generoForm');
    var insertAnio = document.querySelector('#anioForm');

    //AÑADO PELICULA AL JSON
    peliculas.push({
        id: idnum+1,  //AL ULTIMO ID LE SUMO UNO
        nombre: insertTitulo.value,
        genero: insertGenero.value,
        anio: insertAnio.value
    });

    //RESETEO LOS CAMPOS A EMPTY
    insertTitulo.value = '';
    insertGenero.value = '';
    insertAnio.value = '';

    //RELANZO EL BUCLE PARA PINTAR LA TABLA CON LA NUEVA PELÍCULA
    refrescar(); 
    
}

// FUNCION REFRESCAR PARA PINTAR LA TABLA 
function refrescar () {
    for (var i = 0; i < peliculas.length; i++) {

        var bodyTabla = document.querySelector('tbody');
        var fila = document.createElement('tr');

        var idTabla = document.createElement('td');
        var nombreTabla = document.createElement('td');
        var generoTabla = document.createElement('td');
        var anioTabla = document.createElement('td');

        var celdaBoton = document.createElement('td');
        var botonEditar = document.createElement('button');
        var botonBorrar = document.createElement('button');
        
        
        botonEditar.innerText = 'EDITAR';
        botonEditar.className = 'btn-editar';
        botonBorrar.innerText = 'BORRAR';
        botonBorrar.className = 'btn-eliminar';


        //EDITAR PELÍCULA
        botonEditar.onclick = function(eventoUno) {
            estado = EDIT;
            btnGuardar.textContent = 'ACTUALIZAR';

            //console.log(eventoUno.target.parentNode.parentNode.childNodes[4]);
            //DE ESTA FORMA ESTOY IDENTIFICANDO DEL ABUELO DEL EVENTO SU NIETO 4
            var tipoDatoUno = eventoUno.target.parentNode.parentNode.childNodes[1];
            var tipoDatoDos = eventoUno.target.parentNode.parentNode.childNodes[2];
            var tipoDatoTres = eventoUno.target.parentNode.parentNode.childNodes[3];

            var botonBorrarDisabled = eventoUno.target.parentNode.childNodes[1];
            botonBorrarDisabled.disabled = true;

            //ACTUALIZO DATOS DE PELICULA
            actualizarPelicula = function() {

                var cambiarTitulo = document.querySelector('#tituloForm');
                var cambiarGenero = document.querySelector('#generoForm');
                var cambiarAnio = document.querySelector('#anioForm');

                tipoDatoUno.textContent = cambiarTitulo.value;
                tipoDatoDos.textContent = cambiarGenero.value;
                tipoDatoTres.textContent = cambiarAnio.value;

                cambiarTitulo.value = '';
                cambiarGenero.value = '';
                cambiarAnio.value = '';

                //CAMBIO EL ESTADO / BLOQUEO EL BOTON DE BORRAR / DEVUELVO TEXTO AL BOTON GUARDAR-ACTUALIZAR
                estado = LIST;
                botonBorrarDisabled.disabled = false;
                btnGuardar.textContent = 'GUARDAR';

            };

            //RELLENO CAMPOS CON DATOS DE LA TABLA
            var modTitulo = document.querySelector('#tituloForm');
            modTitulo.value = tipoDatoUno.textContent;

            var modGenero = document.querySelector('#generoForm');
            modGenero.value = tipoDatoDos.textContent;

            var modAnio = document.querySelector('#anioForm');
            modAnio.value = tipoDatoTres.textContent;

        };

        //BORRAR PELÍCULA
        botonBorrar.onclick = function(eventoDos) {

            var borrarPelicula = eventoDos.target.parentNode.parentNode;
            borrarPelicula.remove();

        };

        //ASIGNO VALORES
        idTabla.textContent = peliculas[i].id;
        nombreTabla.textContent = peliculas[i].nombre;
        generoTabla.textContent = peliculas[i].genero;
        anioTabla.textContent = peliculas[i].anio;

        //CREO TR Y TD EN EL CUERPO DE LA TABLA
        bodyTabla.appendChild(fila);
        fila.appendChild(idTabla);
        fila.appendChild(nombreTabla);
        fila.appendChild(generoTabla);
        fila.appendChild(anioTabla);

        // AÑADO BOTON A CELDA Y LUEGO CELDA A LA FILA
        celdaBoton.appendChild(botonEditar);
        celdaBoton.appendChild(botonBorrar);
        fila.appendChild(celdaBoton);
    };

    //VACIO LA TABLA PARA QUE AL ACTUALIZAR NO ME DUPLIQUE LAS PELICULAS
    peliculas = [];

};

//LANZO EL BUCLE PARA PINTAR LA TABLA
refrescar();











